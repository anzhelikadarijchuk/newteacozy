<!DOCTYPE html>
<html>
   
<head>
  <title>Tea Cozy</title>
  <link rel="stylesheet" type="text/css" href="<?php echo(get_stylesheet_uri()); ?>">
  <link rel="icon" href="./teacozy/favicon.ico" type="image/x-icon">
</head> 
   
<body>
  
<!-- Header Section -->

  <header>
    <img class="logo" src="teacozy/img-tea-cozy-logo.png" alt="best-tea" />   

  <nav>
    <ul>
      <li><a href="#">Mission</a></li>
      <li><a href="#">Featured Tea</a></li>
      <li><a href="#">Locations</a></li>
    </ul>
  </nav> 
  </header> 

 
<!-- Main -->

<div class="background">
  <img  src="teacozy/hol_wiedenski_alac_ksiazecy_zagan_lubuskie-1024x984.jpg" alt="best-tea" /> 
  <div class="mission"> 
    <h2>Mission</h2>
    <h4>Handpicked, Artisanally Curated, Free Range, Sustainable, Small Batch, Fair Trade, Organic Tea</h4>  
    </div>
</div>    

<!-- Month Gallery -->

<div class="middle-title">
    <h2>Tea of the Month</h2>
    <h4>What's Steeping at The Tea Cozy</h4>  
</div>  

<div class="month-gallery">
  <div class="img-container">
    <img src="teacozy/img-berryblitz.jpg" alt="best-berry-blitz-tea" />
    <h4>Fall Berry Blitz Tea</h4>
    </div>
  <div class="img-container">
    <img src="teacozy/img-spiced-rum.jpg" alt="best-berry-blitz-tea" />
    <h4>Spiced Rum Tea</h4>
  </div>         
  <div class="img-container">
      <img src="teacozy/img-donut.jpg" alt="best-berry-blitz-tea" />
      <h4>Seasonal Donuts</h4>
      </div>
    <div class="img-container">
      <img src="teacozy/img-myrtle-ave.jpg" alt="best-berry-blitz-tea" />
      <h4>Myrtle Ave Tea</h4>
    </div>         
    <div class="img-container">
        <img src="teacozy/img-bedford-bizarre.jpg" alt="best-berry-blitz-tea" />
        <h4>Bedford Bizarre Tea</h4>
      </div>  
</div>

<!-- Locations -->

<div class="locations">
  
  <img src="teacozy/img-locations-background.jpg" alt="best-tea-locations" />
  <h2>Locations</h2>
      <div class="box"> 
      <div class="location-box">
        <h4>Downtown</h4>
        <p>384 West 4th St</p>
        <p>Suite 108</p>
        <p>Portland, Maine</p>
      </div>

      <div class="location-box">
          <h4>East Bayside</h4>
          <p>3433 Phisherman's Avenue</p>
          <p>(Northwest Corner)</p>
          <p>Portland, Maine</p>
      </div>

      <div class="location-box">
          <h4>Oakdale</h4>
          <p>515 Crescent Avenue</p>
          <p>Second Floor</p>
          <p>Portland, Maine</p>
      </div>
      </div>

 </div>

<!-- Bottom -->
 
 <div class="bottom">
   <h2>The Tea Cozy</h2>
   <h5>contact@theteacozy.com</h5>
   <h5>917-555-8904</h5>
 </div>

 <!-- Footer -->

 <footer>
  <h5>copyright The Tea Cozy 2017</h5>
 </footer>

</body>

</html>